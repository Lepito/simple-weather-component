import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'weatherinfo',
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null
    }
  ]
};
