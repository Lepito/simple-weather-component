import { Component, Prop, Element, State } from '@stencil/core';

@Component({
  tag: 'weather-info',
  styleUrl: 'weather-info.css',
  shadow: true
})
export class WeatherInfo {
  @Prop() first: string;
  
  @Element() weatherElem: HTMLElement;
  @State() cityInput: string;
  @State() weatherDataFound: Boolean = false ;
  @State() temp: string;
  @State() pressure : string;
  @State() humidity: string;
  @State() icon: string;
  @State() windSpeed: string;
  @State() cityName: string;
  @State() weatherDescription: string;
  @State() errorMessage: string ;
  getInputValue(event){
    this.cityInput = event.target.value
  }

  fetchWeatherData(city: string){
    const apikey = ''
    let url = '//api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric'+'&APPID='+apikey

    fetch(url)
    .then(response => response.json())
    .then((data) => {
      //handle data here
      if (data.cod === 200) {
        this.weatherDataFound = true;
        this.temp = data.main.temp;
        this.pressure = data.main.pressure;
        this.humidity = data.main.humidity;
        this.icon = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";
        this.windSpeed = data.wind.speed;
        this.cityName = data.name;
        this.weatherDescription = data.weather[0].description;
      }
      else{
        this.weatherDataFound = false;
        this.errorMessage = data.message;
        this.temp = '';
        this.pressure = '';
        this.humidity =''; 
        this.icon = '';
        this.windSpeed = '';
        this.cityName = '';
        this.weatherDescription = '';
      }
    })
    .catch((err) => {
      console.log(err)
    })
  }

  render() {
    let infodiv = null;
    if (this.weatherDataFound) {
       infodiv = (
        <div class="weather-info">
          <table class="weather-info-table">
            <tbody>
              <tr>
                <td><h2>{this.cityName}</h2></td>
              </tr>
              <tr>
                <td class=""><img class="" id="wicon" src={this.icon} alt="Weather icon"/></td>
              </tr>
              <tr>
                <td>
                <label htmlFor=""><span class="bold-text">Description:</span> {this.weatherDescription}</label>
                </td>
              </tr>
              <tr>
                <td> <label htmlFor=""><span class="bold-text">Temperature:</span> {this.temp} °C</label></td>
                
              </tr>
              <tr>
                <td><label htmlFor=""><span class="bold-text">Pressure:</span> {this.pressure}  hPa</label></td>
              </tr>
              <tr>
                <td><label htmlFor=""><span class="bold-text">Humidity:</span> {this.humidity} %</label></td>
              </tr>
              <tr>
                <td><label htmlFor=""><span class="bold-text">Wind Speed:</span> {this.windSpeed} m/s</label></td>
              </tr>
            </tbody>

          </table>
        </div>
      )
    }
    else{
      infodiv = (
        <div class="weather-info">
          <h1 class="center-text error-message">{this.errorMessage}</h1>
        </div>
      );
    }
    return (
      <div class="wrapper">
        <header class="header"><h1>Simple Weather App</h1></header>
        <div class="input-field">
          <label>City: </label>
          <input class="" type="text" name="city-input" id="city-input" onKeyUp={(event: UIEvent) => this.getInputValue(event)}/>
        </div>
        
          <div class="buttons">
            <button class="" onClick={() => this.fetchWeatherData(this.cityInput)}>Search Weather Data</button>
            <button class="" id="lappeenranta-weather" onClick={() => this.fetchWeatherData('Lappeenranta')}>Lappeenranta Weather</button>
            <button class="" id="helsinki-weather" onClick={() => this.fetchWeatherData('Helsinki')}>Helsinki Weather</button>
          </div>          
          <div class="infodiv">{infodiv}</div>
      </div>
    );
  }
}
